1，jQuery中的Delegate（）函数有什么作用？
Representative（）会在以下两个情况下使用到：
（1）如果你有一个父元素，需要给其下的子元素添加事件，这时你可以使用delegate（）了，代码如下：
$（“ ul”）。delegate（“ li”，“ click”，function（）{
$（this）.hide（）;
}）;
（2）当元素在当前页面中不可用时，可以使用delegate（）；

2，如何用jQuery局部浏览器的前进后退按钮？
$（document）.ready（function（）{
     window.history.forward（1）;
     window.history.forward（-1）;
}）;

3. jQuery 库中的 $() 是什么？（答案如下）
　　	$() 函数是 jQuery() 函数的别称，乍一看这很怪异，还使 jQuery 代码晦涩难懂。一旦你适应了，你会爱上它的简洁。$() 函数用于将任何对象包裹成 jQuery 对象，接着你就被允许调用定义在 jQuery 对象上的多个不同方法。你甚至可以将一个选择器字符串传入 $() 函数，它会返回一个包含所有匹配的 DOM 元素数组的 jQuery 对象。这个问题我已经见过好几次被提及，尽管它非常基础，它经常被用来区分一个开发人员是否了解 jQuery。

4. jQuery 里的 ID 选择器和 class 选择器有何不同？（答案）
　　如果你用过 CSS，你也许就知道 ID 选择器和 class 选择器之间的差异，jQuery 也同样如此。ID 选择器使用 ID 来选择元素，比如 #element1，而 class 选择器使用 CSS class 来选择元素。当你只需要选择一个元素时，使用 ID 选择器，而如果你想要选择一组具有相同 CSS class 的元素，就要用 class 选择器。在面试过程中，你有很大几率会被要求使用 ID 选择器和 class 选择器来写代码。下面的 jQuery 代码使用了 ID 选择器和 class 选择器：

5.  网页上有 5 个 <div> 元素，如何使用 jQuery来选择它们？
　　另一个重要的 jQuery 问题是基于选择器的。jQuery 支持不同类型的选择器，例如 ID 选择器、class 选择器、标签选择器。鉴于这个问题没提到 ID 和 class，你可以用标签选择器来选择所有的 div 元素。jQuery 代码：$("div")，这样会返回一个包含所有 5 个 div 标签的 jQuery 对象。更详细的解答参见上面链接的文章。



6.为什么要使用jQuery？你觉得jquery有哪些好处？
  1、因为jQuery是轻量级的框架，大小不到30kb
  2、它有强大的选择器，出色的DOM操作的封装
  3、有可靠的事件处理机制(jQuery在处理事件绑定的时候相当的可靠)
  4、完善的ajax(它的ajax封装的非常的好，不需要考虑复杂浏览器的兼容性和       XMLHttpRequest对象的创建和使用的问题。)
  5、出色的浏览器的兼容性
  6、支持链式操作，隐式迭代

7.$(document).ready() $(functiion({}))方法和window.onload有什么区别？
   1、window.onload方法是在网页中所有的元素完全加载到浏览器后才执行
   2、$(document).ready() 可以在DOM载入就绪是就对其进行操纵，并调用执行绑   定的函数

8. $(document).ready() 是个什么函数？为什么要用它？(answer)
　　这个问题很重要，并且常常被问到。 ready() 函数用于在文档进入ready状态时执行代码。当DOM 完全加载（例如HTML被完全解析DOM树构建完成时），jQuery允许你执行代码。使用$(document).ready()的最大好处在于它适用于所有浏览器，jQuery帮你解决了跨浏览器的难题。需要进一步了解的用户可以点击 answer链接查看详细讨论。
9. 你是如何将一个 HTML 元素添加到 DOM 树中的？（答案如下）

你可以用 jQuery 方法 appendTo() 将一个 HTML 元素添加到 DOM 树中。这是 jQuery 提供的众多操控 DOM 的方法中的一个。你可以通过 appendTo() 方法在指定的 DOM 元素末尾添加一个现存的元素或者一个新的 HTML 元素。

10. 你能用 jQuery 代码选择所有在段落内部的超链接吗？

这是另一个关于选择器的 jQuery 面试题。就像其他问题那样，只需一行 jQuery 代码就能搞定。你可以使用下面这个 jQuery 代码片段来选择所有嵌套在段落（<p>标签）内部的超链接（<a>标签）

11.你知道jquery中的选择器吗，请讲一下有哪些选择器？
选择器大致分为:基本选择器，层次选择器，过滤选择器，表单选择器

12.jquery中的选择器 和 css中的选择器有区别吗？
jQuery选择器支持CSS里的选择器，jQuery选择器可用来添加样式和添加相应的行为，CSS 中的选择器是只能添加相应的样式。

13.jquery对象和dom对象是怎样转换的？
jquery转DOM对象:jQuery 对象是一个数组对象，可以通过[index]的丰富得到相应的DOM对象，还可以通过get[index]去得到相应的DOM对象；DOM对象转jQuery对象:$(DOM对象)

14.你是如何使用jquery中的ajax的？
如果是一些常规的ajax程序的话，使用load(),.get(),.post(),就可以搞定了，一般我会使用的是.post()方法。如果需要设定beforeSend(提交前回调函数),error(失败后处理),success(成功后处理)及complete(请求完成后处理)回调函数等，这个时候我会使用.ajax()。
15.你觉得beforeSend方法有什么用？
发送请求前可以修改XMLHttpRequest对象的函数，在beforeSend中，如果返回false可以取消本次的Ajax请求。XMLHttpRequest对象是唯一的参数，所以在这个方法里可以做验证

16.jquery中的load方法一般怎么用的？
load方法一般在载入远程HTML代码并插入到DOM中的时候用，通常用来从Web服务器上获取静态的数据文件。如果要传递参数的话，可以使用.get()或.post()

17.你在ajax中使用过JSON吗，你是如何用的？
使用过，在.getJSON()方法的时候就是，因为.getJSON() 就是用于加载JSON文件的