## 原生js ajax请求步骤

```js
//创建 XMLHttpRequest 对象
var ajax = new XMLHttpRequest();
//规定请求的类型、URL 以及是否异步处理请求。
ajax.open('GET/POST',url,true);
//发送信息至服务器时内容编码类型
ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
//发送请求
ajax.send(null);  
//接受服务器响应数据
ajax.onreadystatechange = function () {
    if (obj.readyState == 4 && (obj.status == 200 || obj.status == 304)) { 
    }
};
```

#### XMLHttpRequest兼容

```js
var ajax;
if (window.XMLHttpRequest){
    //  IE7+, Firefox, Chrome, Opera, Safari
  	ajax=new XMLHttpRequest();
}
else{
    // code for IE6, IE5
  	ajax=new ActiveXObject("Microsoft.XMLHTTP");
}
```

#### 将异步改为同步

```js
ajax.open('GET/POST',url,true/false);
// 第三个参数为false 时 为同步  默认true
```

#### XMLHttpRequest作用

- 从服务器获取数据
- 向服务器提交数据
- 达到不刷新页面而局部更新

#### http状态码

- 200				请求成功
- 201				上传文件成功
- 301				永久重定向（redirect）
- 302，307				临时重定向（redirect）
- 304				浏览器缓存
- 403				请求不到首页，没有权限
- 404				请求的资源在前端查明不存在
- 405				请求方法不支持
- 500				服务器的内部错误，程序错误
- 502				请求的资源前端有记录指向后端数据库，却找不到后端资源
- 503				服务暂时不可用
- 504				请求超时

#### 常用的HTTP方法

- GET： 用于请求访问已经被URI（统一资源标识符）识别的资源，可以通过URL传参给服务器
- POST：用于传输信息给服务器，主要功能与GET方法类似，但一般推荐使用POST方式。
- PUT： 传输文件，报文主体中包含文件内容，保存到对应URI位置。
- HEAD： 获得报文首部，与GET方法类似，只是不返回报文主体，一般用于验证URI是否有效。
- DELETE：删除文件，与PUT方法相反，删除对应URI位置的文件。
- OPTIONS：查询相应URI支持的HTTP方法。

#### Ajax的状态码 || 生命周期

​	0	请求初始化	

​			代理被创建 但是open未调用

​	1	服务器连接已建立

​			open已经调用	建立连接

​	2	请求已接收

​			send方法已被调用 可获取状态行和响应头

​	3	请求处理中

​			响应下载中	可能有部分数据

​	4	请求已完成	响应已就绪

​			响应下载完成	数据完整