### 面试题

1. for in 和Object.keys 都是用来循环对象的，二者有什么区别？
   - 二者均能遍历对象的可枚举属性；
   - for in只能遍历obj对象自身的属性，Object.keys可遍历obj对象隐式原型上的属性;
   - Object.keys返回的是一个由对象的key组成的数组，for in无返回值；

2. 对象合并：Object.assign (目标对象,源对象,源对象)；obj={...obj1,...obj2}; 

3. 给obj对象新增一个sex属性，值为women，读取时，值为女？

   通过Object的访问器属性中的getter实现

4. 设置obj对象所有属性不可被 删除

   Object.seal(obj) //密封

5. 设置obj对象不可被扩展

   Object.preventExtensions(obj) //不可被扩展

6. 设置obj对象所有属性不可被修改

   Object.freeze(obj)//冻结 
   
7. 如何在JS中动态添加/删除对象的属性？

   可以使用object.属性 = value相对性添加属性，delete object.属性 来删除属性

   ```js
   let user =new Object();
   user.name = 'Job';
   user.age = 25;
   console.log(user);//{name:'Job',age:25}
   delete user.age;
   console.log(user);//{name:'Job'}
   ```
   
8. 如何在JS中克隆对象？

   Object.assign ( ) 方法用于在JS中克隆对象。

   ```js
   var x = {myProp:'value'};
   var y = Object.assign({},x);
   ```

9. 如何将JS日期转化为ISO标准？

   toISOString ( ) 方法用于将js日期转换为ISO标准。它使用ISO标准将js Date对象转换为字符串。

   ```js
   var date = new Date();
   var n = date.toISOString();
   console.log(n);
   ```

10. JS中的substr ( ) 和substring ( ) 方法有什么区别？

    substr ( startIndex,length ) 。从startIndex返回字符串并返回 'length'个字符串。

    substring ( startIndex,endIndex )。返回从startIndex到 endIndex-1 的字符串。

    ```js
    var s = 'hello';
    console.log(s.substr(1,4));//ello
    console.log(s.substring(1,4));//ell
    ```

11. 解释一下“use strict”。

    "use strict"是Es5中引入的js指令。使用"use strict"指令的目的是强制执行严格模式下的代码。在严格模式下，不能在不声明变量的情况下使用变量。早期版本的js忽略了"use strict"。

12. 如何将文件的所有导出作为一个对象？

    import * as objectname from './file.js' 用于将所有导出的成员导入为对象。可以使用对象的点（.）运算符来访问导出的变量或方法。

13. JS中的宿主对象与原生对象有何不同？

    宿主对象：这些事运行环境提供的对象。这意味着他们在不同环境下是不同的。例如：浏览器包含像window这样的对象，但是Node.js环境提供像Node List这样的对象。

    原生对象：这些是JS中的内置对象。它们也被称为全局对象，因为如果使用JS，内置对象不受是运行环境影响。
