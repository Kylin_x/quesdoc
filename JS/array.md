### **面试题**

##### 数组去重

###### 第一种方法：

先对数组进行排序 sort()，排好序，然后把数组的当前项和后一项进行比较，相同则使用数组的 splice(相同的位置，1)，但是为了防止数组塌陷，每次删除数组元素的时候要把 i 的值减一。

```js
var ary = [1, 23, 4, 2, 33, 1, 2, 44, 3, 2, 3];
ary.sort(function (a, b) {
  return a - b;
}); //返回的是排好序的数组
for (var i = 0; i < ary.length; i++) {
  if (ary[i] === ary[i + 1]) {
    ary.splice(i, 1);
    i--; //为了防止数组塌陷
  }
}
```

###### 第二种方法是：

​	建立一个新数组，要是原数组里面的数是头一次出现（使用数组的 indexOf（）），那么就把这个数放到新数组里面，否者就抛弃这个数，类似于前面说的随机验证码

```js
var ary=[1,23,4,2,33,1,2,44,3,2,3]

var arr2=[];
for(var i=0;i<ary.length;i++){
if(arr2.indexOf(art[i])==-1){
arr2.push(ary[i]);}
```

###### 第三种方法：

​		直接拿当前项与后一项进行比较，相同的要删除,使用数组 splice()进行删除，这里也要防止数组塌陷；

```js
var ary = [1, 23, 4, 2, 33, 1, 2, 44, 3, 2, 3];
for (var i = 0; i < ary.length; i++) {
  for (var j = i + 1; j < ary.length; j++) {
    if (ary[i] === ary[j]) {
      ary.splice(j, 1);
      j--;
    }
  }
}
```

###### 第四种方法：

​		利用对象不重名的特性，对象的存储是键值对方式，要获取属性值，要通过对象./[]属性来获取；相同则删除，也要防止数组塌陷

```js
var ary = [1, 23, 4, 2, 33, 1, 2, 44, 3, 2, 3];
//新建一个对象
var obj = {};
for (var i = 0; i < ary.length; i++) {
  var cur = ary[i];
  //如果 obj【cur】为真，那就说明对象里面找到了这个数，重复了
  if (obj[cur]) {
    ary.splice(i, 1);
    i--;
  } else {
    //给对象赋值 说明 obj.xxx=undefined 不存在为假
    obj[cur] = cur; //obj【1】=1；
  }
}
```

###### 第五种方法：

​	也是利用对象不重名的方法，但是这次是直接计算其数组各个数的重复次数，

```js
var ary = [1, 23, 4, 2, 33, 1, 2, 44, 3, 2, 3];
var obj = {};
for (var i = 0; i < ary.length; i++) {
  var cur = ary[i];
  if (obj[cur]) {
    //每次重复一次，其对应的值加一
    obj[cur]++;
  } else {
    //说明没有重复，个数只有一个
    obj[cur] = 1;
  }
}
var ary2 = [];
//对于对象使用 in 方法进行遍历，遍历获取的是属性值
for (var attr in obj) {
  ary2.push(Number(attr));
}
```

###### 6.利用 ES6 Set 去重（ES6 中最常用）

```js
function unique(arr) {
  return Array.from(new Set(arr));
}
var arr = [
  1,
  1,
  "true",
  "true",
  true,
  true,
  15,
  15,
  false,
  false,
  undefined,
  undefined,
  null,
  null,
  NaN,
  NaN,
  "NaN",
  0,
  0,
  "a",
  "a",
  {},
  {},
];
console.log(unique(arr)); //[1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {}, {}]
```

###### 7.利用 includes

```js
function unique(arr) {
  if (!Array.isArray(arr)) {
    console.log("type error!");
    return;
  }
  var array = [];
  for (var i = 0; i < arr.length; i++) {
    if (!array.includes(arr[i])) {
      //includes 检测数组是否有某个值
      array.push(arr[i]);
    }
  }
  return array;
}
var arr = [
  1,
  1,
  "true",
  "true",
  true,
  true,
  15,
  15,
  false,
  false,
  undefined,
  undefined,
  null,
  null,
  NaN,
  NaN,
  "NaN",
  0,
  0,
  "a",
  "a",
  {},
  {},
];
console.log(unique(arr));
//[1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {…}, {…}]     //{}没有去重
```

###### 八、利用 filter

```js
function unique(arr) {
  return arr.filter(function (item, index, arr) {
    //当前元素，在原始数组中的第一个索引==当前索引值，否则返回当前元素
    return arr.indexOf(item, 0) === index;
  });
}
var arr = [
  1,
  1,
  "true",
  "true",
  true,
  true,
  15,
  15,
  false,
  false,
  undefined,
  undefined,
  null,
  null,
  NaN,
  NaN,
  "NaN",
  0,
  0,
  "a",
  "a",
  {},
  {},
];
console.log(unique(arr));
//[1, "true", true, 15, false, undefined, null, "NaN", 0, "a", {…}, {…}]
```

###### 9、利用 Map 数据结构去重

```js
function arrayNonRepeatfy(arr) {
  let map = new Map();
  let array = new Array(); // 数组用于返回结果
  for (let i = 0; i < arr.length; i++) {
    if (map.has(arr[i])) {
      // 如果有该key值
      map.set(arr[i], true);
    } else {
      map.set(arr[i], false); // 如果没有该key值
      array.push(arr[i]);
    }
  }
  return array;
}
var arr = [
  1,
  1,
  "true",
  "true",
  true,
  true,
  15,
  15,
  false,
  false,
  undefined,
  undefined,
  null,
  null,
  NaN,
  NaN,
  "NaN",
  0,
  0,
  "a",
  "a",
  {},
  {},
];
console.log(unique(arr));
//[1, "a", "true", true, 15, false, 1, {…}, null, NaN, NaN, "NaN", 0, "a", {…}, undefined]
```

创建一个空 Map 数据结构，遍历需要去重的数组，把数组的每一个元素作为 key 存到 Map 中。由于 Map 中不会出现相同的 key 值，所以最终得到的就是去重后的结果。

###### 10、利用 reduce+includes

```js
function unique(arr) {
  return arr.reduce(
    (prev, cur) => (prev.includes(cur) ? prev : [...prev, cur]),
    []
  );
}
var arr = [
  1,
  1,
  "true",
  "true",
  true,
  true,
  15,
  15,
  false,
  false,
  undefined,
  undefined,
  null,
  null,
  NaN,
  NaN,
  "NaN",
  0,
  0,
  "a",
  "a",
  {},
  {},
];
console.log(unique(arr));
// [1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {…}, {…}]
```

###### 11、[...new Set(arr)]

```js
[...new Set(arr)];
```

//代码就是这么少----（其实，严格来说并不算是一种，相对于第一种方法来说只是简化了代码）





### 0529

 一、将字符串反向 'abc123' => '321cba'

'abc123'.split('').reverse().join('')

 二、打平嵌套数组 [1, [2, [3], 4], 5] => [1, 2, 3, 4, 5]

1、 const arr = [1,[2,[3],4],5]
function flatten(arr) {
    for (let i in arr) {
        if (Array.isArray(arr[i])) {
            arr.splice(i, 1, ...flatten(arr[i]))
        }
    }
    return arr
}
flatten(arr)

2、偶然发现 arr.toString() 或 arr.join() => '1,2,3,4,5'

const arr = [1,[2,[3],4],5]
arr.join()
	.split(',')
	.map(it => Number(it))

3、 const arr = [1, [2, [3], 4], 5]
		JSON.parse(`[${arr}]`)



###### 三、 JS中的Array.splice()和Array.slice()方法有什么区别

话不多说，来看第一个例子：

```
var arr=[0,1,2,3,4,5,6,7,8,9];//设置一个数组
console.log(arr.slice(2,7));//2,3,4,5,6
console.log(arr.splice(2,7));//2,3,4,5,6,7,8
//由此我们简单推测数量两个函数参数的意义,
slice(start,end)第一个参数表示开始位置,第二个表示截取到的位置(不包含该位置)
splice(start,length)第一个参数开始位置,第二个参数截取长度
```

接着看第二个：

```
var x=y=[0,1,2,3,4,5,6,7,8,9]
console.log(x.slice(2,5));//2,3,4
console.log(x);[0,1,2,3,4,5,6,7,8,9]原数组并未改变
//接下来用同样方式测试splice
console.log(y.splice(2,5));//2,3,4,5,6
console.log(y);//[0,1,7,8,9]显示原数组中的数值被剔除掉了
```

slice和splice虽然都是对于数组对象进行截取,但是二者还是存在明显区别,函数参数上slice和splice第一个参数都是截取开始位置,slice第二个参数是截取的结束位置(不包含),而splice第二个参数(表示这个从开始位置截取的长度),slice不会对原数组产生变化,而splice会直接剔除原数组中的截取数据!







